<?php

    require_once "models/CVInfo.php";
    require_once "models/Competence.php";
    require_once "models/Project.php";

    require_once "controllers/SkillController.php";
    require_once "controllers/CVInfoController.php";
    require_once "controllers/ProjectController.php";

    require_once "init.php";

    include "views/header.php";

    include "views/formation.inc.php";
    include "views/experience.inc.php";
    include "views/competence.inc.php";
    include "views/project.inc.php";

    include "views/footer.php";
