
// Return boolean when an element is visible on screen
function isVisible(element) {
    let
        viewPortHeight = $(window).height(), // Viewport Height
        scrollTop = $(window).scrollTop(), // Scroll Top
        currElementPosY = $(element).offset().top,
        elementHeight = $(element).height();

    return (currElementPosY + elementHeight > scrollTop && currElementPosY < (viewPortHeight + scrollTop));
}

// Animate progress bar only when they're visible
function animateBar () {
    $(".bar-competence").each(function(){

        let percent = parseInt($(this).attr("id").split("-")[2]) / 100;

        progressBar("#" + $(this).attr("id"), percent);
    });

    progressbarVisible = true;
}

function progressBar(id, percent){
    let bar = new ProgressBar.Circle(id, {
        duration: 3000,
        trailColor: '#eee',
        easing: 'easeInOut',
        strokeWidth: 10,
        trailWidth: 4,
        text: {
            autoStyleContainer: false
        },
        from: { color: '#294e47', width: 1 },
        to: { color: '#88bdab', width: 8 },
        step: function(state, circle) {
            circle.path.setAttribute('stroke', state.color);
            circle.path.setAttribute('stroke-width', state.width);
            // circle.text.

            let value = Math.round(circle.value() * 100);
            if (value === 0) {
                circle.setText('');
            } else {
                circle.setText(value+'%');
            }
        }
    });

    $('.progressbar-text').removeAttr('style');

    bar.animate(percent);
}

function hireMe(){
    console.log('██╗  ██╗██╗██████╗ ███████╗    ███╗   ███╗███████╗         ');
    console.log('██║  ██║██║██╔══██╗██╔════╝    ████╗ ████║██╔════╝         ');
    console.log('███████║██║██████╔╝█████╗      ██╔████╔██║█████╗           ');
    console.log('██╔══██║██║██╔══██╗██╔══╝      ██║╚██╔╝██║██╔══╝           ');
    console.log('██║  ██║██║██║  ██║███████╗    ██║ ╚═╝ ██║███████╗██╗██╗██╗');
    console.log('╚═╝  ╚═╝╚═╝╚═╝  ╚═╝╚══════╝    ╚═╝     ╚═╝╚══════╝╚═╝╚═╝╚═╝');
    console.log("");
    console.log("romain.duminil@gmail.com");
}
