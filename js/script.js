/**
 * Crée par Romain Duminil le 04/02/2017.
 */

$(document).ready(function(){
    hireMe();

    particlesJS.load('particles-js', 'js/particlesjs-config.json', null);

    let $progressbar = $("#competence");
    let progressbarVisible = false;

    // On scroll
    $(window).scroll(function(){
        if(!progressbarVisible && isVisible($progressbar)){ // si les progress ne sont pas déjà affiché
            animateBar();
            progressbarVisible = true;
        }
    });

    // On load
    if( isVisible($progressbar) ){
        animateBar();
        progressbarVisible = true;
    }

    let $vignettes = $(".vignette-projet");

    $vignettes.hover(function(){
        $(this).children('div').children(".projet-description").slideToggle("slow");
    });
});
