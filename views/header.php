<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="author" content="Romain Duminil" />
    <meta http-equiv="Content-Language" content="fr-FR" />
    <meta name="robots" content="index, follow, archive" />
    <meta name="robots" content="all" />
    <meta name="description" content="CV en ligne - Romain Duminil" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="style.css">
    <link rel="icon" href="img/icon.ico" />
    <title>Romain DUMINIL</title>
</head>
<body>
    <h1 class="nom">Romain DUMINIL<!-- est le plus beau -->
        <span class="subtitle"><?= $CVInfoController->getCurrentJobTitle() ?></span>
    </h1>
    <div id="particles-js" class="particule"></div>
    <div class="home-picture">
        <p><a href="mailto:romain@duminil.eu">romain@duminil.eu</a></p>
        <img src="img/photo-profil.jpg" alt="photo-profil"z>
        <p>06 79 60 62 83</p>
    </div>
