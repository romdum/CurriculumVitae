<div class="competence">
    <header>
        <h2>Compétences</h2>
    </header>
    <div id="competence" class="bloc-progress-bar">
        <?php
        foreach($SkillController->getArraySkill() as $competence):
            ?>
            <div>
                <h3><?= $competence->get_title(); ?></h3>
                <div id="container-<?= $SkillController->getSkillId( $competence ) . "-" . $competence->get_percent(); ?>" class="bar-competence"></div>
            </div>
            <?php
        endforeach;
        ?>
    </div>
</div>
