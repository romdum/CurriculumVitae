<div class="cv">
    <div class="formation">
        <header>
            <h2>Formations</h2>
        </header>

        <?php
        foreach($CVInfoController->getArrayFormation() as $Formation): ?>

        <article class="animated <?= $CVInfoController->getAnimationName( $Formation ) ?>">
            <header>
                <h3><?= $Formation->get_title() ?></h3>
            </header>
            <p class="ecole">
                <?= $Formation->get_date() . " - " . $Formation->get_lieu() ?>
            </p>
            <p>
                <?php
                foreach($Formation->get_content() as $info):
                ?>
                    <p><?= $info ?></p>
                <?php
                endforeach;
                ?>
            </p>
        </article>
        <?php
        endforeach;
        ?>

    </div>
