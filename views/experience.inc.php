    <div class="experience">
        <header>
            <h2>Expériences</h2>
        </header>
        <?php
        foreach($CVInfoController->getArrayExperience() as $experience): ?>

        <article class="animated <?= $CVInfoController->getAnimationName( $experience ) ?>">
            <header>
                <h3><?= $experience->get_title() ?></h3>
            </header>
            <p class="ecole">
                <?= $experience->get_date() . " - " . $experience->get_lieu() ?>
            </p>
            <p>
                <?php
                foreach($experience->get_content() as $info):
                ?>
                    <p><?= $info ?></p>
                <?php
                endforeach;
                ?>
            </p>
        </article>
        <?php
        endforeach;
        ?>
    </div>
</div>
