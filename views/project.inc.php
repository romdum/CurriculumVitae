<div class="projets">
    <header>
        <h2>Projets</h2>
    </header>
    <div class="groupe-vignette">
        <?php foreach( $ProjectController->getArrayProjects() as $Project ): ?>
        <div class="vignette-projet">
            <div style="min-height:250px;background:url('img/bpe.jpg') no-repeat;background-position:center">
                <div class="projet-description hidden">
                    <?php if( $Project->has_customer() ): ?>
                        <p>
                            <u>Client :</u>
                            <?= $Project->get_customer(); ?>
                        </p>
                    <?php endif ?>
                    <p>
                        <u>Mission :</u>
                        <?= $Project->get_description(); ?>
                    </p>
                    <?php if( $Project->has_source() ): ?>
                    <p>
                        <u>Source :</u>
                        <a href="<?= $Project->get_source() ?>"><?= $Project->get_domain_source() ?></a>
                    </p>
                    <?php endif; ?>
                </div>
            </div>
            <header>
                <?php if( $Project->get_link() !== '' ): ?>
                <h3><a href="<?= $Project->get_link() ?>"><?= $Project->get_title(); ?></a></h3>
                <?php else : ?>
                <h3><?= $Project->get_title(); ?></h3>
                <?php endif;?>
            </header>
        </div>
    <?php endforeach; ?>
    </div>
</div>
