<?php

    /**
     * Skills declaration
     */
    $SkillController = new SkillController();

    $SkillController->addSkill( new Competence( "HTML CSS", 90 ) );
    $SkillController->addSkill( new Competence( "PHP", 85 ) );
    $SkillController->addSkill( new Competence( "SQL", 80 ) );
    $SkillController->addSkill( new Competence( "Wordpress", 70 ) );
    $SkillController->addSkill( new Competence( "JS", 70 ) );
    $SkillController->addSkill( new Competence( "Linux", 70 ) );
    $SkillController->addSkill( new Competence( "Docker", 65 ) );
    $SkillController->addSkill( new Competence( "Android", 60 ) );
    $SkillController->addSkill( new Competence( "Java", 60 ) );


    $CVInfoController = new CVInfoController();

    /**
     * Experience declaration
     */
    $experience = new CVInfo( "Ingénieur d'études et développement", "Mars 2018 / Aujourd'hui", "Apside TOP", "experience");
    $experience->addContent( "Développement du site de la MGEN avec Typo3" );
    $CVInfoController->addCVInfo( $experience );

    $experience = new CVInfo( "Ingénieur d'études", "Oct. 2017 / Fev. 2018", "OPEN", "experience" );
    $experience->addContent( "Développement du logiciel PRIMPROMO sous WinDev 22" );
    $CVInfoController->addCVInfo( $experience );

    $experience = new CVInfo( "Analyste programmeur", "Oct. 2016 / Sept. 2017", "EURICIEL", "experience" );
    $experience->addContent( "Développement du logiciel GIMI sous WinDev 20" );
    $experience->addContent( "Développements sous WebDev et WinDev Mobile" );
    $experience->addContent( "Développement d'un espace client web PHP" );
    $CVInfoController->addCVInfo( $experience );

    $experience = new CVInfo( "Stagiaire comptable", "Sept. 2014 / Oct. 2014", "H&M Expertise", "experience" );
    $experience->addContent( "Saisie comptable" );
    $CVInfoController->addCVInfo( $experience );

    $experience = new CVInfo( "Stagiaire comptable", "Mai 2014 / Juin 2014", "CERFRANCE", "experience" );
    $experience->addContent( "Saisie comptable" );
    $CVInfoController->addCVInfo( $experience );

    /**
     * Formation declarion
     */
    $formation = new CVInfo( "Développeur logiciel", "2016 / 2017", "CEFIM", "formation" );
    $formation->addContent( "Développement web : HTML5, CSS3, JS, PHP, Wordpress" );
    $formation->addContent( "Développement logiciel : Java" );
    $formation->addContent( "Développement smartphone : Android" );
    $CVInfoController->addCVInfo( $formation );

    $formation = new CVInfo( "Diplôme de comptabilité et de gestion (Non obtenu)", "2015 / 2016", "IUT de Tours", "formation" );
    $formation->addContent( "Comptabilité approfondie" );
    $formation->addContent( "Comptabilité financière" );
    $formation->addContent( "Fiscalité" );
    $formation->addContent( "Droit des sociétés" );
    $CVInfoController->addCVInfo( $formation );

    $formation = new CVInfo( "BTS Comptabilité et Gestion des Organisations (Mention Bien)", "2013 / 2015", "Lycée Blaise Pascal à Châteauroux", "formation" );
    $formation->addContent( "Comptabilité générale" );
    $formation->addContent( "Fiscalité" );
    $formation->addContent( "Comptabilité de gestion" );
    $formation->addContent( "Droit social" );
    $CVInfoController->addCVInfo( $formation );

    $formation = new CVInfo( "Baccalauréat STG (Mention Bien)", "2013 / 2015", "Lycée Blaise Pascal à Châteauroux", "formation" );
    $CVInfoController->addCVInfo( $formation );

    /**
     * Project declaration
     */
    $ProjectController = new ProjectController();

    $Project = new Project( 'Budget Participatif Etudiant', 'img/bpe.jpg' );
    $Project->add_description( 'Développement du plugin <strong>CommunityProject</strong> ajoutant un CPT <em>projet</em>, ainsi qu\'un User Role <em>étudiant</em>.' );
    $Project->add_description( 'Développement du plugin <strong>CommunityVoice</strong> permettant de voter sur les projets.' );
    $Project->add_description( '<br><u>Compétences acquises :</u> WordPress (Custom Post Type, Gestion des droits ...)' );
    $Project->set_source( 'https://framagit.org/Artefacts/CommunityVoice' );
    $Project->set_link( 'http://bpe.univ-tours.fr/' );
    $Project->set_customer( 'Université François Rabelais de Tours' );
    $ProjectController->addProject( $Project );
