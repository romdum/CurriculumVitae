<?php

class Project
{
    private $title;
    private $imageUrl;
    private $description;
    private $link;
    private $customer;
    private $source;

    function __construct( $title = '', $imageUrl = '', $description = '', $link = '' )
    {
        $this->title = $title;
        $this->imageUrl = $imageUrl;
        $this->description = $description;
        $this->link = $link;
    }

    function get_title()
    {
        return $this->title;
    }

    function get_imageUrl()
    {
        return $this->imageURL;
    }

    function get_description()
    {
        return $this->description;
    }

    function get_link()
    {
        return $this->link;
    }

    function get_customer()
    {
        return $this->customer;
    }

    function get_source()
    {
        return $this->source;
    }

    function get_domain_source()
    {
        return explode( '/', $this->source )[2];
    }

    function set_title( $title )
    {
        $this->title = $title;
    }

    function set_imageUrl( $imageUrl )
    {
        $this->imageUrl = $imageUrl;
    }

    function set_description( $description )
    {
        $this->description = $description;
    }

    function add_description( $descrition )
    {
        $this->description .= $descrition . '<br>';
    }

    function set_link( $link )
    {
        $this->link = $link;
    }

    function set_customer( $customer )
    {
        $this->customer = $customer;
    }

    function set_source( $source )
    {
        $this->source = $source;
    }

    function has_source()
    {
        return $this->source === '' || $this->source === null ? false : true;
    }

    function has_link()
    {
        return $this->link === '' || $this->link === null ? false : true;
    }

    function has_customer()
    {
        return $this->customer === null || $this->customer === '' ? false : true;
    }
}
