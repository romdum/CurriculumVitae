<?php

class Competence
{

    private $title;
    private $percent;

    function __construct($title,$percent)
    {
        $this->title = $title;
        $this->percent = $percent;
    }

    function get_title()
    {
        return $this->title;
    }

    function get_percent()
    {
        return $this->percent;
    }
}
