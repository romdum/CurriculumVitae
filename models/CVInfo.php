<?php

class CVInfo
{

    private $title, $date, $lieu, $type;
    private $content = [];

    function __construct( $title, $date = '', $lieu = '', $type = '' )
    {
        $this->title = $title;
        $this->date  = $date;
        $this->lieu  = $lieu;
        $this->type  = $type;
    }

    function get_type()
    {
        return $this->type;
    }

    function get_title()
    {
        return $this->title;
    }

    function get_date()
    {
        return $this->date;
    }

    function get_lieu()
    {
        return $this->lieu;
    }

    function get_content()
    {
        return $this->content;
    }

    function addContent( $content )
    {
        $this->content[] = $content;
    }

    function setDate( $date )
    {
        $this->date = $date;
    }

    function setLieu( $lieu )
    {
        $this->lieu = $lieu;
    }


}
