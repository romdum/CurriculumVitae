<?php

class CVInfoController
{
    private $cvInfos;

    function __construct( $cvInfos = null )
    {
        if( $cvInfos !== null && is_array( $cvInfos ) )
        {
            $this->cvInfos = $cvInfos;
        }
    }

    function addCVInfo( $CVInfo )
    {
        $this->cvInfos[] = $CVInfo;
    }

    function getArrayFormation()
    {
        $formations = [];

        foreach( $this->cvInfos as $formation )
        {
            if( $formation->get_type() === 'formation' )
            {
                $formations[] = $formation;
            }
        }

        return $formations;
    }

    function getArrayExperience()
    {
        $experiences = [];

        foreach( $this->cvInfos as $experience )
        {
            if( $experience->get_type() === 'experience' )
            {
                $experiences[] = $experience;
            }
        }

        return $experiences;
    }

    function getAnimationName( $CVInfo )
    {
        if( $CVInfo->get_type() === 'formation' )
        {
            return 'fadeInLeft';
        }
        else
        {
            return 'fadeInRight';
        }
    }

    function getCurrentJobTitle()
    {
        return $this->getArrayExperience()[0]->get_title();
    }
}
