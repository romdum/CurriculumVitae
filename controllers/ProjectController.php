<?php

class ProjectController
{
    private $projects = [];

    public function __construct( $projects = null )
    {
        if( $projects !== null && is_array( $projects ) )
            $this->projects = $projects;
    }

    public function getArrayProjects()
    {
        return $this->projects;
    }

    public function addProject( $Project )
    {
        $this->projects[] = $Project;
    }

    public function getCountProject()
    {
        return count( $this->projects );
    }
}
