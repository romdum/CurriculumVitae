<?php

class SkillController
{
    private $competences;

    function __construct( $competences = null )
    {
        if( $competences !== null && is_array( $competences ) )
        {
            $this->competences = $competences;
        }
    }

    function addSkill( $skill )
    {
        $this->competences[] = $skill;
    }

    function getSkillCount()
    {
        return count( $this->competences );
    }

    function getArraySkill()
    {
        return $this->competences;
    }

    function getSkillId( $competence )
    {
        return str_replace( ' ', '', $competence->get_title() );
    }
}
